"""Seed the database with dummy data"""

from uuid import uuid4
from random import randrange, sample
from typing import List
import sys

from sqlalchemy.orm import Session

from mnemo.database import engine
from mnemo.models import Update


def main() -> None:
    nbr_of_updates = 10

    if len(sys.argv) > 1:
        try:
            int(sys.argv[1])
        except Exception:
            print(f"Not a number: {sys.argv[1]}")
            exit(1)

        nbr_of_updates = int(sys.argv[1])

    updates = create_updates(nbr_of_updates)
    save_updates(updates)


def save_updates(updates: List[Update]) -> None:
    session = Session(engine)

    for update in updates:
        session.add(update)

    session.commit()


def create_updates(number: int) -> List[Update]:
    return [create_update(i) for i in range(number)]


def create_update(number: int) -> Update:
    uuid = uuid4()
    hostname = f"hostname_{number}"
    snapshot_name = f"snapshot_name_{number}"
    snapshot_active = True if randrange(2) else False

    nbr_of_packages = randrange(5)
    packages = get_packages(nbr_of_packages)

    return Update(
        uuid=str(uuid),
        hostname=hostname,
        snapshot_name=snapshot_name,
        snapshot_active=snapshot_active,
        packages=packages,
    )


def get_packages(number: int) -> List[str]:
    packages = (
        "vim",
        "tree",
        "fzf",
        "python3",
        "curl",
        "wget",
        "bat",
        "rg",
        "iputils-ping",
        "nmap",
    )

    return sample(packages, k=number)


if __name__ == "__main__":
    main()
