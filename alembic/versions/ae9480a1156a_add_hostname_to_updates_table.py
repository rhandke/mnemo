"""add_hostname_to_updates_table

Revision ID: ae9480a1156a
Revises: 5a227a04bbd4
Create Date: 2022-01-13 17:52:21.271414

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ae9480a1156a'
down_revision = '5a227a04bbd4'
branch_labels = None
depends_on = None

TABLE_NAME = 'updates'


def upgrade():
    with op.batch_alter_table(TABLE_NAME, ) as batch_op:
        batch_op.add_column(sa.Column('hostname', sa.String(50), nullable=False))


def downgrade():
    with op.batch_alter_table(TABLE_NAME,) as batch_op:
        batch_op.drop_column('hostname')
