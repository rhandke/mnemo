"""create update table

Revision ID: 5a227a04bbd4
Revises: 
Create Date: 2022-01-11 17:52:24.378155

"""
from datetime import datetime

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5a227a04bbd4'
down_revision = None
branch_labels = None
depends_on = None

TABLE_NAME='updates'


def upgrade():
    op.create_table(
        TABLE_NAME,
        sa.Column('id', sa.Integer, primary_key=True, index=True),
        sa.Column('uuid', sa.String(36), index=True, nullable=False),
        sa.Column('snapshot_name', sa.String(50), nullable=False),
        sa.Column('snapshot_active', sa.Boolean, default=True, nullable=False),
        sa.Column('packages', sa.JSON, nullable=False),
        sa.Column('updated_at', sa.DateTime, default=datetime.utcnow, nullable=False),
    )


def downgrade():
    op.drop_table(TABLE_NAME)
