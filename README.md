![tests](https://gitlab.com/rhandke/mnemo/badges/main/pipeline.svg)

# Mnemo

The little brother of Mnemosyne, the greek godess of memory, helps with updates. He exposes an API which allows CRUD operations on update objects.

# Installation

Clone the repo

```
git clone <link> <path-to-repo>
cd <path-to-repo>
```

(Optional) Create and activate a virtual environment (Requires virtualenv to be installed)
```
virtualenv venv
source venv/bin/activate
```

Install flit
```
pip3 install flit
```

Install the application
```
flit install
```

# Configuration
Configuration of the database is handled via environment variables (or .env file)
```
export DATABASE_URL="postgresql://user:password@postgresserver/db"
# export DATABASE_URL="sqlite:///./sql_app.db"
```

When using an sqlite Database it is also required to set the DB_TYPE environment variable
```
export DB_TYPE="sqlite"
```

# Usage
Launch the Uvicorn server
```
uvicorn mnemo.main:app --reload
```

For production the reload flag is not necessary. The host and port flags allow may need to be set, however.
```
uvicorn mnemo.main:app --host <ip> --port <port>
```

## Routes
The app comes with a openAPI documentation of the available routes
```
http://127.0.0.1:8000/docs
```

There is also a redoc version available
```
http://127.0.0.1:8000/redoc
```

# Development

There is a docker-compose.yml available for development purposes. It contains three services:
1. The API - localhost:8000
2. A Postgresql Database
3. Adminer (to allow easy access to the Database via the Browser) - localhost:8080

## Prerequisites
- docker
- docker-compose

## Setup
Start the docker-compose environment
```
docker-compose up -d
```

Create the database tables using alembic.
```
docker container exec mnemo_api alembic upgrade head
```

The API should now answer on localhost:8000/updates
Adminer should answer on localhost:8080 to allow access to the database

## Seeding
While the database is up, use the seed.py script inside the api container to seed the database with update data

```
docker container exec mnemo_api python3 seed.py <number_of_updates> # default is 10
```
