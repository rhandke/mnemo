from datetime import datetime
from pathlib import Path

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import mnemo.crud as c
import mnemo.schemas as s
import mnemo.models as m
from mnemo.models import Base


@pytest.fixture
def session():
    db_url = "sqlite:///./sql_test.db"
    engine = create_engine(db_url, connect_args={"check_same_thread": False})
    SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    Base.metadata.create_all(bind=engine)
    db = SessionLocal()
    db_update = m.Update(
        uuid="418d04eb-85ca-41a7-9dcf-db7b66b554b1",
        hostname="hostname_1",
        snapshot_name="snapshot_name_1",
        snapshot_active=True,
        packages=["vim", "fzf", "tree"],
    )
    db.add(db_update)
    db_update = m.Update(
        uuid="418d04eb-85ca-41a7-9dcf-db7b66b554b1",
        hostname="hostname_2",
        snapshot_name="snapshot_name_2",
        snapshot_active=True,
        packages=["vim", "fzf", "tree"],
    )
    db.add(db_update)
    db.commit()
    db.refresh(db_update)
    try:
        yield db
    finally:
        db.close()
    file = Path(__file__).parent.parent.absolute() / "sql_test.db"
    file.unlink()


def test_get_update_by_uuid_returns_newest_entry(session):
    uuid = "418d04eb-85ca-41a7-9dcf-db7b66b554b1"
    hostname = "hostname_2"
    name = "snapshot_name_2"
    active = True
    packages = ["vim", "fzf", "tree"]

    result = c.get_update_by_uuid(session, uuid)
    if not result:
        raise ValueError(f"No update found with uuid {uuid}")
    if result:
        assert uuid == result.uuid
        assert hostname == result.hostname
        assert name == result.snapshot_name
        assert active == result.snapshot_active
        assert packages == result.packages


def test_get_update_by_id(session):
    uuid = "418d04eb-85ca-41a7-9dcf-db7b66b554b1"
    hostname = "hostname_1"
    name = "snapshot_name_1"
    active = True
    packages = ["vim", "fzf", "tree"]
    id = 1
    result = c.get_update_by_id(session, id)
    if not result:
        raise ValueError(f"No update found with id {id}")
    if result:
        assert uuid == result.uuid
        assert hostname == result.hostname
        assert name == result.snapshot_name
        assert active == result.snapshot_active
        assert packages == result.packages


@pytest.mark.parametrize(
    "skip, limit, uuid, expected",
    [
        (
            0,
            100,
            None,
            [
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_2",
                    "name": "snapshot_name_2",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_1",
                    "name": "snapshot_name_1",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
            ],
        ),
        (
            0,
            100,
            "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
            [
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_2",
                    "name": "snapshot_name_2",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_1",
                    "name": "snapshot_name_1",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
            ],
        ),
        (
            1,
            100,
            None,
            [
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_1",
                    "name": "snapshot_name_1",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
            ],
        ),
        (
            0,
            1,
            None,
            [
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_2",
                    "name": "snapshot_name_2",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
            ],
        ),
        (
            1,
            100,
            "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
            [
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_1",
                    "name": "snapshot_name_1",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
            ],
        ),
        (
            0,
            1,
            "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
            [
                {
                    "uuid": "418d04eb-85ca-41a7-9dcf-db7b66b554b1",
                    "hostname": "hostname_2",
                    "name": "snapshot_name_2",
                    "active": True,
                    "packages": ["vim", "fzf", "tree"],
                },
            ],
        ),
    ],
)
def test_get_updates(session, skip, limit, uuid, expected):
    results = c.get_updates(session, skip, limit, uuid)

    for i in range(0, len(expected)):
        assert results[i].uuid == expected[i]["uuid"]
        assert results[i].hostname == expected[i]["hostname"]
        assert results[i].snapshot_name == expected[i]["name"]
        assert results[i].snapshot_active == expected[i]["active"]
        assert results[i].packages == expected[i]["packages"]


def test_create_update_returns_correct_value(session):
    uuid = "418d04eb-85ca-41a7-9dcf-db7b66b554b1"
    hostname = "hostname_3"
    name = "snapshot_name_3"
    active = True
    packages = ["vim", "fzf", "tree"]
    update = s.UpdateCreate(
        uuid=uuid,
        hostname=hostname,
        snapshot_name=name,
        snapshot_active=active,
        packages=packages,
    )
    result = c.create_update(session, update)

    assert result.uuid == uuid
    assert result.snapshot_name == name
    assert result.snapshot_active == active
    assert result.packages == packages


def test_create_update_creates_database_entry(session):
    uuid = "418d04eb-85ca-41a7-9dcf-db7b66b554b1"
    hostname = "hostname_3"
    name = "snapshot_name_3"
    active = True
    packages = ["vim", "fzf", "tree"]
    update = s.UpdateCreate(
        uuid=uuid,
        hostname=hostname,
        snapshot_name=name,
        snapshot_active=active,
        packages=packages,
    )
    c.create_update(session, update)

    db_update = c.get_update_by_uuid(session, uuid)
    if db_update:
        assert db_update.id == 3
        assert db_update.uuid == uuid
        assert db_update.snapshot_name == name
        assert db_update.snapshot_active == active
        assert db_update.packages == packages


def test_update_update_returns_correct_value(session):
    update = s.Update(
        id=1,
        uuid="418d04eb-85ca-41a7-9dcf-db7b66b554b1",
        hostname="hostname_3",
        snapshot_name="updated name",
        snapshot_active=False,
        packages=["new", "awesome", "package"],
        updated_at=datetime.utcnow(),
    )
    result = c.update_update(session, update, update.id)
    if result:
        assert result.id == update.id
        assert result.hostname == update.hostname
        assert result.uuid == update.uuid
        assert result.snapshot_name == update.snapshot_name
        assert result.snapshot_active == update.snapshot_active
        assert result.packages == update.packages


def test_update_update_modifies_database_entry_correctly(session):
    update = s.Update(
        id=1,
        uuid="418d04eb-85ca-41a7-9dcf-db7b66b554b1",
        hostname="hostname_3",
        snapshot_name="updated name",
        snapshot_active=False,
        packages=["new", "awesome", "package"],
        updated_at=datetime.utcnow(),
    )
    c.update_update(session, update, update.id)

    result = c.get_update_by_id(session, 1)
    if result:
        assert result.id == update.id
        assert result.uuid == update.uuid
        assert result.hostname == update.hostname
        assert result.snapshot_name == update.snapshot_name
        assert result.snapshot_active == update.snapshot_active
        assert result.packages == update.packages


def test_delete_update_returns_object(session):
    uuid = "418d04eb-85ca-41a7-9dcf-db7b66b554b1"
    name = "snapshot_name_1"
    active = True
    packages = ["vim", "fzf", "tree"]
    id = 1
    result = c.delete_update(session, id)
    assert result is not None
    assert result.uuid == uuid
    assert result.snapshot_name == name
    assert result.snapshot_active == active
    assert result.packages == packages


def test_delete_update_returns_none_if_no_db_entry_is_found(session):
    assert c.delete_update(session, 55) is None


def test_delete_update_removes_db_entry(session):
    id = 1
    c.delete_update(session, id)
    assert c.get_update_by_id(session, id) is None
