from datetime import datetime

from sqlalchemy import Boolean, Column, Integer, String, DateTime, JSON
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Update(Base):  # type: ignore
    __tablename__ = "updates"

    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String(36), index=True, nullable=False)
    hostname = Column(String(50), nullable=False)
    snapshot_name = Column(String(50), nullable=False)
    snapshot_active = Column(Boolean, default=True, nullable=False)
    packages = Column(JSON, nullable=False)
    updated_at = Column(DateTime, default=datetime.utcnow, nullable=False)
