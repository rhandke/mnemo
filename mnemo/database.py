from pathlib import Path
import os
from dotenv import dotenv_values

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

PROJECT_ROOT = Path(__file__).parent.parent.absolute()

config = {**dotenv_values(PROJECT_ROOT / ".env"), **os.environ}

connect_args = {}
if "DB_TYPE" in config and config["DB_TYPE"] == "sqlite":
    connect_args = {"check_same_thread": False}

if "DATABASE_URL" not in config:
    print("No database configured")
    exit(1)

engine = create_engine(config["DATABASE_URL"], connect_args=connect_args)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
