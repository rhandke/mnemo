from typing import Optional, List, Union

from sqlalchemy.orm import Session

from mnemo import models, schemas


def get_update_by_uuid(db: Session, uuid: str) -> Union[schemas.Update, None]:
    return (
        db.query(models.Update)
        .filter(models.Update.uuid == uuid)
        .order_by(models.Update.updated_at.desc())
        .first()
    )


def get_update_by_id(db: Session, id: int) -> Union[schemas.Update, None]:
    return db.query(models.Update).filter(models.Update.id == id).first()


def get_updates(
    db: Session, skip: int = 0, limit: int = 100, uuid: Optional[str] = None
) -> List[models.Update]:
    if uuid:
        return (
            db.query(models.Update)
            .filter(models.Update.uuid == uuid)
            .order_by(models.Update.updated_at.desc())
            .offset(skip)
            .limit(limit)
            .all()
        )
    return (
        db.query(models.Update)
        .order_by(models.Update.updated_at.desc())
        .offset(skip)
        .limit(limit)
        .all()
    )


def create_update(db: Session, update: schemas.UpdateCreate) -> schemas.Update:
    db_update = models.Update(
        uuid=update.uuid,
        hostname=update.hostname,
        snapshot_name=update.snapshot_name,
        snapshot_active=update.snapshot_active,
        packages=update.packages,
    )
    db.add(db_update)
    db.commit()
    db.refresh(db_update)
    return db_update


def update_update(
    db: Session, update: schemas.Update, id: int
) -> Optional[schemas.Update]:
    db_update = db.query(models.Update).filter(models.Update.id == id).first()
    if db_update:
        db_update.uuid = update.uuid
        db_update.hostname = update.hostname
        db_update.snapshot_name = update.snapshot_name
        db_update.snapshot_active = update.snapshot_active
        db_update.packages = update.packages
        db.commit()
    return db_update


def delete_update(db: Session, id: int) -> Union[schemas.Update, None]:
    update = get_update_by_id(db, id)
    if update:
        db.delete(update)
        db.commit()
    return update
