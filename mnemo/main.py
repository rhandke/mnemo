from typing import List, Optional

from fastapi import FastAPI, Depends, HTTPException
from sqlalchemy.orm import Session

from mnemo.database import SessionLocal
from mnemo import crud, schemas

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/updates", response_model=List[schemas.Update])
def index(
    skip: int = 0,
    limit: int = 100,
    uuid: Optional[str] = None,
    db: Session = Depends(get_db),
):
    updates = crud.get_updates(db, skip=skip, limit=limit, uuid=uuid)
    return updates


@app.post("/updates", response_model=schemas.Update)
def create(update: schemas.UpdateCreate, db: Session = Depends(get_db)):
    return crud.create_update(db=db, update=update)


@app.get("/updates/{id}", response_model=schemas.Update)
def read(id: int, db: Session = Depends(get_db)):
    db_update = crud.get_update_by_id(db, id)
    if not db_update:
        raise HTTPException(
            status_code=404, detail=f"Could not find update with id: {id}"
        )
    return db_update


@app.patch("/updates/{id}", response_model=schemas.Update)
def update(id: int, update: schemas.Update, db: Session = Depends(get_db)):
    result = crud.update_update(db, update, id)
    if not result:
        raise HTTPException(
            status_code=404, detail=f"Could not find update with id: {id}"
        )
    return result


@app.delete("/updates/{id}", response_model=schemas.Update)
def destroy(id: int, db: Session = Depends(get_db)):
    result = crud.delete_update(db, id)
    if result is None:
        raise HTTPException(
            status_code=404, detail=f"Could not find update with id: {id}"
        )
    return result
