from typing import List, Union, Dict, Any
from datetime import datetime

from pydantic import BaseModel


class UpdateBase(BaseModel):
    uuid: str
    hostname: str
    snapshot_name: str
    snapshot_active: bool
    packages: Union[Dict[str, Any], List[Any]]


class UpdateCreate(UpdateBase):
    pass


class Update(UpdateBase):
    id: int
    updated_at: datetime

    class Config:
        orm_mode = True
