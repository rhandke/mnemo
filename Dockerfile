FROM python:3.8.12-slim

WORKDIR /app
COPY . /app

RUN export FLIT_ROOT_INSTALL=1 \
  && pip install --no-cache-dir --upgrade -r requirements.txt \
  && flit install

EXPOSE 8000

CMD ["uvicorn", "mnemo.main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]
